# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
import aiohttp
from loguru import logger
import requests

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter


class ScrapycompositedemoSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class ScrapycompositedemoDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class AuthorizationMiddleware():
    accountpool_url = 'http://127.0.0.1:5000/'
    # authorization = 'jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyNiwidXNlcm5hbWUiOiJhZG1pbjEiLCJleHAiOjE2NTE1MjczNDcsImVtYWlsIjoiIiwib3JpZ19pYXQiOjE2NTE0ODQxNDd9.j4-cFajkUfryys43viQwSzTedg9g6HFb1e90cClmY8M'

    # async def process_request(self, request, spider):
    #     async with aiohttp.ClientSession() as client:
    #         res = await client.get(self.accountpool_url)
    #         if not res.status == 200:
    #             return
    #         authorization = await res.text()
    #         authorization = f'jwt {authorization}'
    #         logger.info(res.text(), authorization)
    #     request.headers['authorization'] = authorization
    tunnel = "tps656.kdlapi.com:15818"
    username = "t15160185660737"
    password = "vjopqphj"
    proxies = "http://%(user)s:%(pwd)s@%(proxy)s/" % {
        "user": username, "pwd": password, "proxy": tunnel}

    def process_request(self, request, spider):
        with requests.get(self.accountpool_url) as res:
            authorization = res.text
            authorization = f'jwt {authorization}'
            request.headers['authorization'] = authorization
            # print(f'***********{res.status_code}**{authorization}***********')
            # request.meta['proxy'] = self.proxies
